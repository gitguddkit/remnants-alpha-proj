﻿// SDK Object Alias|Utilities|90063
namespace VRTK
{
    using UnityEngine;

    /// <summary>
    /// The GameObject that the SDK Object Alias script is applied to will become a child of the selected SDK Object.
    /// </summary>
    public class VRTK_DroneBodyAlias : MonoBehaviour
    {
        
        /// <summary>
        /// Valid SDK Objects
        /// </summary>
        /// <param name="Boundary">The main camera rig/play area object that defines the player boundary.</param>
        /// <param name="Headset">The main headset camera defines the player head.</param>
        public enum SDKObject
        {
            Boundary,
            Headset
        }

        [Tooltip("The specific SDK Object to child this GameObject to.")]
        public SDKObject sdkObject = SDKObject.Boundary;

        protected VRTK_SDKManager sdkManager;

        protected virtual void OnEnable()
        {
            this.enabled = true;
            sdkManager = VRTK_SDKManager.instance;
            if (sdkManager != null)
            {
                sdkManager.LoadedSetupChanged += LoadedSetupChanged;
            }
            ChildToSDKObject();
        }

        protected virtual void OnDisable()
        {
            if (sdkManager != null && !gameObject.activeSelf)
            {
                sdkManager.LoadedSetupChanged -= LoadedSetupChanged;
            }
        }

        protected virtual void LoadedSetupChanged(VRTK_SDKManager sender, VRTK_SDKManager.LoadedSetupChangeEventArgs e)
        {
            if (sdkManager != null)
            {
                ChildToSDKObject();
            }
        }

        protected virtual void ChildToSDKObject()
        {
            Vector3 currentPosition = transform.localPosition;
            Quaternion currentRotation = transform.localRotation;
            Vector3 currentScale = transform.localScale;
            Transform newParent = null;

            switch (sdkObject)
            {
                case SDKObject.Boundary:
                    newParent = VRTK_DeviceFinder.PlayAreaTransform();
                    break;
                case SDKObject.Headset:
                    newParent = VRTK_DeviceFinder.HeadsetTransform();
                    break;
            }
            // TL - looking at the Y axis in Euler angles is not enough, we need the look direction, AKA a unit vector that corresponds to the direction the player is facing. 
            // The problem is that if the players headset is straight up, then it rotates around the Y axis, however, if the players headset is sideways (Z axis by 90 deg), then the look direction is the on the X axis.
            // To fix this i wrote code that will average between the X and Y Axis depending on the Z rotation. 
            // logic: 
            // P = Z % 180 
            // Q = abs(P - 90) 
            // (Y / 90 * Q) + (X / 90 * Q)
            //newParent.localRotation.SetLookRotation(
            //    new Vector3(0.0f, 0.0f, (transform.localRotation.eulerAngles.y / 90.0f * Mathf.Abs((transform.localRotation.eulerAngles.z % 180.0f) - 90.0f)) 
            //        + (transform.localRotation.eulerAngles.x / 90.0f * (Mathf.Abs((transform.localRotation.eulerAngles.z % 180.0f) - 90.0f)))),
            //    Vector3.up
            //);
            
            transform.SetParent(newParent, false);
            //currentPosition.y -= 5.0f;
            transform.localPosition = currentPosition;
            transform.localRotation = currentRotation; // TL - assign the edited rotation.
            transform.localScale = currentScale;
        }

        void Update()
        {
            transform.eulerAngles = new Vector3(0.0f, 0.0f, (transform.eulerAngles.y / 90.0f * Mathf.Abs((transform.eulerAngles.z % 180.0f) - 90.0f))
                    + (transform.eulerAngles.x / 90.0f * (Mathf.Abs((transform.eulerAngles.z % 180.0f) - 90.0f))));
            //this.transform.rotation.SetLookRotation(
            //    new Vector3(0.0f, 0.0f, (this.transform.localRotation.eulerAngles.y / 90.0f * Mathf.Abs((this.transform.localRotation.eulerAngles.z % 180.0f) - 90.0f))
            //        + (this.transform.localRotation.eulerAngles.x / 90.0f * (Mathf.Abs((this.transform.localRotation.eulerAngles.z % 180.0f) - 90.0f)))),
            //    Vector3.up
            //);
        }
    }
}