﻿using UnityEngine;
using VRTK;

public sealed class DroneCameraRigSync : MonoBehaviour { // TL - Adapted this code to work for an ingame object that uses our imported models.
    public GameObject mDroneHead;
    public GameObject mDroneBody;
    // public GameObject RightHand;

    private void OnEnable() {
        SetUpTransformFollow(mDroneBody, VRTK_DeviceFinder.Devices.Headset);
        //SetUpTransformFollow(mDroneBody, VRTK_DeviceFinder.Devices.RightController);
        SetUpHorizontalAxisTransformFollow(mDroneHead, VRTK_DeviceFinder.Devices.Headset);
        // SetUpTransformFollow(RightHand, VRTK_DeviceFinder.Devices.RightController);
        //SetUpControllerHandLink(mDroneBody, VRTK_DeviceFinder.Devices.RightController);
        // SetUpControllerHandLink(RightHand, VRTK_DeviceFinder.Devices.RightController);
    }

    private static void SetUpTransformFollow(GameObject avatarComponent, VRTK_DeviceFinder.Devices device) {
        var photonView = avatarComponent.GetComponent<PhotonView>();
        if (photonView == null) {
            Debug.LogError(string.Format("The network representation '{0}' has no {1} component on it.", avatarComponent.name, typeof(PhotonView).Name));
            return;
        }

        if (!photonView.isMine) {
            return;
        }

        var transformFollow = avatarComponent.AddComponent<VRTK_TransformFollow>();
        transformFollow.gameObjectToFollow = VRTK_DeviceFinder.DeviceTransform(device).gameObject;
        
        transformFollow.followsScale = false;
    }

    // TL - i wrote this method to take just XZ position updates and Y Rotation so that the body of the character ccould properly follow the players movements. 
    private static void SetUpHorizontalAxisTransformFollow(GameObject avatarComponent, VRTK_DeviceFinder.Devices device) 
    {
        var photonView = avatarComponent.GetComponent<PhotonView>();
        if (photonView == null)
        {
            Debug.LogError(string.Format("The network representation '{0}' has no {1} component on it.", avatarComponent.name, typeof(PhotonView).Name));
            return;
        }

        if (!photonView.isMine)
        {
            return;
        }

        var transformFollow = avatarComponent.AddComponent<VRTK_TransformFollow>();
        //transformFollow.followsRotation = false;
        transformFollow.gameObjectToFollow = VRTK_DeviceFinder.DeviceTransform(device).gameObject;
        //transformFollow.followsRotation = false;
        transformFollow.followsScale = false;
    }


    //    GameObject avatarComponent, VRTK_DeviceFinder.Devices device)
    //{
    //    var photonView = avatarComponent.GetComponent<PhotonView>();
    //    if (photonView == null)
    //    {
    //        Debug.LogError(string.Format("The network representation '{0}' has no {1} component on it."));
    //        return;
    //    }

    //    if (!photonView.isMine)
    //    {
    //        return;
    //    }

    //    var transformFollow = avatarComponent.AddComponent<VRTK_TransformFollow>();
    //    // needs the look direction for following rotation, aka, the direction on the Y Axis the user is pointing to. 
    //    transformFollow.gameObjectToFollow = VRTK_DeviceFinder.DeviceTransform(device).gameObject;
    //    //transformFollow.;
    //    //transformFollow.smoothsPosition = true;
    //    //transformFollow.followsRotation = false;
    //    transformFollow.followsScale = false;
    //}

    private static void SetUpControllerHandLink(GameObject avatarComponent, VRTK_DeviceFinder.Devices device) {
        var photonView = avatarComponent.GetComponent<PhotonView>();
        if (photonView == null) {
            Debug.LogError(string.Format("The network representation '{0}' has no {1} component on it.", avatarComponent.name, typeof(PhotonView).Name));
            return;
        }

        if (!photonView.isMine) {
            return;
        }

        GameObject controller = VRTK_DeviceFinder.DeviceTransform(device).gameObject;
        GameObject actual = VRTK_DeviceFinder.GetActualController(controller);
        var link = actual.AddComponent<PhotonViewLink>();
        link.linkedView = photonView;
    }
}
