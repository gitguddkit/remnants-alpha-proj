﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class killTouch : MonoBehaviour {

    public Text loseText;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("player"))
        {
            other.gameObject.SetActive(false);
            Time.timeScale = 0;
            loseText.text = "YOU LOST!";
        }
    }
}
