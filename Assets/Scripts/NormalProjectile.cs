﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NormalProjectile : BaseProjectile
{

    Vector3 m_direction;
    bool m_fired;
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (m_fired)
        {
            transform.position += m_direction * (speed * Time.deltaTime);
        }

    }
    public override void FireProjectile(GameObject launcher, GameObject target, int damage)
    {
        if (launcher && target)
        {
            m_direction = (target.transform.position - launcher.transform.position).normalized;
            m_fired = true;
        }
    }
    
}
