﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;


public class Patrol : MonoBehaviour
{

    public Transform[] points;
    public Transform player;
    int moveSpeed = 2;
    int maxDist = 6;
    int minDist = 5;
    private bool alert = false;
    private int destPoint = 0;
    private NavMeshAgent agent;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;
        GotoNextPoint();
    }


    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
    }


    void Update()
    {
        if (Vector3.Distance(transform.position, player.position) <= maxDist)
        {
            alert = true;
        }
        else { alert = false; }
        // Choose the next destination point when the agent gets
        // close to the current one.
        if (alert == false)
        {
            if (!agent.pathPending && agent.remainingDistance < 0.5f)
                GotoNextPoint();
        }
        else if (alert == true)
        {
            // if (!agent.pathPending && agent.remainingDistance < 0.5f)
            // GotoNextPoint();
            agent.destination = player.position;
            transform.LookAt(player);
        }
    }
}